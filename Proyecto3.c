//Camilo Alviña Baos 18.584.961-9

#include <stdio.h>
#include <stdlib.h>
#include "universidad.h"

void ConsultaPonderacion(carrera pondera[]){
int opcion,COD,i=0; //Definicion de variables
char CARR[100];

		printf( "\n\t   1. Buscar Carrera por Nombre." );	//Menú para elegir opción de busqueda de carrera.
		printf( "\n\t   2. Buscar Carrera por Codigo." );
		printf( "\n\t   3. Volver al menú." );
		printf( "\n\n\t   Introduzca opción (1-3): " );
		scanf("%d",&opcion);
		switch ( opcion ){			//Pedimos al usuario que ingrese el nombre de la Carrera siguiendo las indicaciones dadas.
				case 1: printf("\n\tIngrese nombre de la Carrera. Si el nombre lleva espacios modifiquelos por guion bajo(_) (EJ. Ingeniería_Ambiental):\n \t");
						scanf("%s",&CARR);//Almacenamos la respuesta en CARR.
						printf("\n\tFACULTAD CARRERA CODIGO NEM RANKING LENG MAT HIST CS POND PSU PTJMAX PTJMIN CUPOPSU CUPOBEA\n\n");//Hacemos un printf de guía con los campos que se mostrarán.
						for (i=0;i<53;i++){//Bucle for para recorrer el archivo txt.
							if(strcmp(CARR,pondera[i].CARRERA)==0){//Con strcmp comparamos la respuesta almacenada en CARR con pondera[i].CARRERA, si coincide imprimirá las ponderaciones de la carrera solicitada.
								printf("\t%s %s %d %d %d %d %d %d %d %d %d %.2f %.2f %d %d \n",pondera[i].FACULTAD,pondera[i].CARRERA,pondera[i].CODIGO,pondera[i].NEM,pondera[i].RANK,pondera[i].LENG,pondera[i].MAT,pondera[i].HIST,pondera[i].CS,pondera[i].POND,pondera[i].PSU,pondera[i].PTJMAX,pondera[i].PTJMIN,pondera[i].CUPOPSU,pondera[i].CUPOBEA);
							}
						}
						break;
								//Pedimos al usuario que ingrese el Codigo de la Carrera.
				case 2:  printf("\n\tIngrese Codigo de la Carrera (EJ. Para aquitectura - 19020):\n \t");
						scanf("%d",&COD);//Almacenamos la respuesta en COD.
						printf("\n\tFACULTAD CARRERA CODIGO NEM RANKING LENG MAT HIST CS POND PSU PTJMAX PTJMIN CUPOPSU CUPOBEA\n\n");//Hacemos un printf de guía con los campos que se mostrarán.
						for (i=0;i<53;i++){////Bucle for para recorrer el archivo txt.
							if(COD==pondera[i].CODIGO){//Si el valor de COD es igual al valor encontrado en la estrutura imprimira las ponderaciones de la carrera.
								printf("\t%s %s %d %d %d %d %d %d %d %d %d %.2f %.2f %d %d \n",pondera[i].FACULTAD,pondera[i].CARRERA,pondera[i].CODIGO,pondera[i].NEM,pondera[i].RANK,pondera[i].LENG,pondera[i].MAT,pondera[i].HIST,pondera[i].CS,pondera[i].POND,pondera[i].PSU,pondera[i].PTJMAX,pondera[i].PTJMIN,pondera[i].CUPOPSU,pondera[i].CUPOBEA);	
							}
						}
						break;						
	    }
}
void SimularPostulacion(){
	printf("Test");
}
void MostrarPonderacion(carrera pondera[]){
int i=0;//Definicion de variables para el bucle for. 
char FAC[50];//Variable para almacenar la respuesta del usuario.
		//Pedimos al usuario que ingrese el nombre de la Facultad siguiendo las indicaciones que se muestran. 
		printf("\t   Ingrense nombre de la Facultad que desea consultar en MAYUSCULAS (EJ Para buscar Facultad de Medicina se debe poner: MEDICINA):\n \t");
		scanf("%s",&FAC);//Almacenamos respuesta en FAC.
		printf("\n\tFACULTAD CARRERA CODIGO NEM RANKING LENG MAT HIST CS POND PSU PTJMAX PTJMIN CUPOPSU CUPOBEA\n\n");//Hacemos un printf de guía con los campos que se mostrarán. 
		for (i=0;i<53;i++){//Bucle for que recorre las 53 lineas de nuestro txt.
			if(strcmp(FAC,pondera[i].FACULTAD)==0){//Con strcmp comparamos la respuesta almacenada en FAC con pondera[i].FACULTAD, si coincide imprimirá las ponderaciones.
				printf("\t%s %s %d %d %d %d %d %d %d %d %d %.2f %.2f %d %d \n",pondera[i].FACULTAD,pondera[i].CARRERA,pondera[i].CODIGO,pondera[i].NEM,pondera[i].RANK,pondera[i].LENG,pondera[i].MAT,pondera[i].HIST,pondera[i].CS,pondera[i].POND,pondera[i].PSU,pondera[i].PTJMAX,pondera[i].PTJMIN,pondera[i].CUPOPSU,pondera[i].CUPOBEA);
			}
		}
}
void menu(carrera pondera[]){
FILE *file; //Creamos puntero en el archivo.
int i = 0;
	if((file=fopen("carreras.txt","r"))==NULL){//Abrimos el archivo en modo leer, si es nulo imprime error.
		printf("\nError al arbir el archivo");
		exit(0); 
	}
	else{//En caso contrario, recorre el archivo con un bucle for.
		for (i=0;i<53;i++){//Recorremos las 53 lineas del archivo.
			//Con fscanf buscamos una entrada en el archivo y le asigamos una estructura.
			fscanf(file,"%s %s %d %d %d %d %d %d %d %d %d %f %f %d %d", pondera[i].FACULTAD,&pondera[i].CARRERA,&pondera[i].CODIGO,&pondera[i].NEM,&pondera[i].RANK,&pondera[i].LENG,&pondera[i].MAT,&pondera[i].HIST,&pondera[i].CS,&pondera[i].POND,&pondera[i].PSU,&pondera[i].PTJMAX,&pondera[i].PTJMIN,&pondera[i].CUPOPSU,&pondera[i].CUPOBEA);
		
		}
		fclose(file); //Cerramos archivo.
	}  
	int opcion;
    do{		
		printf( "\n\t   1. Consultar Ponderación Carrera." );	//Menú principal del programa.
		printf( "\n\t   2. Simular Postulación Carrera." );
		printf( "\n\t   3. Mostar Ponderaciones Facultad." );
		printf( "\n\t   4. Salir." );
		printf( "\n\n\t   Introduzca opción (1-4): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: ConsultaPonderacion(pondera); 
					break;

			case 2:  SimularPostulacion(); 
					break;

			case 3: MostrarPonderacion(pondera);
					break;
         }

    } while ( opcion != 4 );
}

int main(){;
	carrera pondera[53];
	menu(pondera); 
	return EXIT_SUCCESS; 
}
